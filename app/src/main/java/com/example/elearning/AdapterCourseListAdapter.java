package com.example.elearning;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.text.format.DateFormat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AlertDialogLayout;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.Calendar;

public class AdapterCourseListAdapter extends RecyclerView.Adapter<AdapterCourseListAdapter.viewHolder> implements View.OnClickListener {

    String tv_res;
    private Context context;
    private ArrayList<ModelCourse> videoArrayList;
    public AdapterCourseListAdapter(Context context, ArrayList<ModelCourse> videoArrayList) {
        this.context = context;
        this.videoArrayList = videoArrayList;
    }


    @NonNull
    @Override
    public viewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.course_list_admin, parent, false);
        return new viewHolder(view);
    }


    @Override
    public void onBindViewHolder(@NonNull viewHolder holder, int position) {
        ModelCourse modelVideo = videoArrayList.get(position);
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(Long.parseLong(modelVideo.getTimestamp()));
        String formattedDate = DateFormat.format("dd/MM/yyyy", calendar).toString();

        holder.tv_title.setText(modelVideo.getTitle());
        holder.tv_time.setText(formattedDate);
        holder.tv_category.setText(modelVideo.getCategory());
        holder.tv_totalLessons.setText(modelVideo.getTotalLessons() + " Lessons");
        holder.tv_tutor.setText(modelVideo.getTutor());
        holder.tv_res.setText(modelVideo.getCourse_res());
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                tv_res = modelVideo.getCourse_res();
                Intent intent = new Intent(context,PlayCourse.class);
                intent.putExtra("titleTv",modelVideo.getTutor());
                intent.putExtra("categoryTv",modelVideo.getTitle());
                intent.putExtra("timeTv",modelVideo.getTimestamp());
                intent.putExtra("totlaLessonsTv",modelVideo.getTotalLessons());
                intent.putExtra("categoryTv",modelVideo.getTitle());
                intent.putExtra("tv_res",tv_res);
                context.startActivity(intent);
                }
        });
    }

    @Override
    public int getItemCount() {
        return 0;
    }

    @Override
    public void onClick(View v) {

    }


    class viewHolder extends RecyclerView.ViewHolder{

        TextView tv_title, tv_time, tv_totalLessons, tv_category, tv_tutor,tv_res;
        ImageView iv_delete;

        public viewHolder(@NonNull View itemView) {
            super(itemView);

            tv_title = itemView.findViewById(R.id.titleTv);
            tv_time = itemView.findViewById(R.id.timeTv);
            tv_totalLessons = itemView.findViewById(R.id.totlaLessonsTv);
            tv_category = itemView.findViewById(R.id.categoryTv);
            tv_tutor = itemView.findViewById(R.id.tutorTv);
            iv_delete = itemView.findViewById(R.id.iv_delete);
        }
    }
}


