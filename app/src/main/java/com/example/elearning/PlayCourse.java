package com.example.elearning;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.widget.MediaController;
import android.widget.VideoView;

public class PlayCourse extends AppCompatActivity {

    VideoView videoView;
    String url;
    MediaController mediaController;
    String mv_res;

//    public Vedioplay(String mv_res){
//        this.mv_res = mv_res;
//    }

//    public String getMv_res() {
//        return mv_res;
//    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_play_course);
        videoView = findViewById(R.id.activity_vedio_play);
        Intent intent =getIntent();
        mv_res = intent.getStringExtra("mv_res");

//        url = "android.resource://" + getPackageName() + "/raw/"+ mv_res;
        url = "android.resource://com.example.elearning/raw/"+mv_res;
        Uri uri = Uri.parse(url);
        videoView.setVideoURI(uri);
        mediaController = new MediaController(this);
        videoView.setMediaController(mediaController);
        if(videoView!=null&&videoView.isPlaying()){
            videoView.stopPlayback();
            return;
        }
        videoView.start();
    }

}