package com.example.elearning.weather;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.elearning.R;

import java.util.List;

public class weather_sy extends AppCompatActivity implements View.OnClickListener{
    TextView tv_city,tv_weather,tv_temp,tv_wind,tv_pm;
    ImageView iv_icon;
    Button btn_bj,btn_sh,btn_gz;
    List<WeatherInfo> infoList;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_weather_sy);
        tv_city = findViewById(R.id.tv_city);
        tv_weather = findViewById(R.id.tv_weather);
        tv_temp = findViewById(R.id.tv_temp);
        tv_wind = findViewById(R.id.tv_wind);
        tv_pm = findViewById(R.id.tv_pm);
        iv_icon = findViewById(R.id.iv_icon);
        btn_bj = findViewById(R.id.btn_bj);
        btn_sh = findViewById(R.id.btn_sh);
        btn_gz = findViewById(R.id.btn_gz);

        btn_bj.setOnClickListener(this);
        btn_sh.setOnClickListener(this);
        btn_gz.setOnClickListener(this);

        infoList = WeatherApiClient.getInstance().getInfoFromJson(this);
        getCityData("110100");
    }

    public void setData(WeatherInfo info){
        if(info==null) return;
        tv_city.setText(info.getCity());
        tv_weather.setText(info.getText());
        tv_temp.setText(info.getTemp());
        tv_wind.setText(info.getWind_class());
        tv_pm.setText(info.getPm25());
        if(("晴转多云").equals(info.getText())){
            iv_icon.setImageResource(R.drawable.cloud_sun);
        }
        if(("多云").equals(info.getText())){
            iv_icon.setImageResource(R.drawable.clouds);
        }
        if(("晴").equals(info.getText())){
            iv_icon.setImageResource(R.drawable.sun);
        }
    }

    public void getCityData(String city){
        for(WeatherInfo info : infoList){
            if(info.getCity().equals(city)){
                setData(info);
            }
        }
    }
    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btn_bj:
                getCityData("110100");
                break;

            case R.id.btn_sh:
                getCityData("310100");
                break;

            case R.id.btn_gz:
                getCityData("440100");
                break;
        }
    }
}