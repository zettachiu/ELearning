package com.example.elearning.weather;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.webkit.WebView;

import com.example.elearning.R;

public class weather_web extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_weather_web);
        WebView webView = findViewById(R.id.weather_web);
        webView.loadUrl("https://m.weather.com.cn/d/town/index?lat=39.864792&lon=116.371468");

        webView.getSettings().setSupportZoom(true);
        webView.getSettings().setBuiltInZoomControls(true);
    }
}