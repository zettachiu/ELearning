package com.example.elearning.admin;

import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.example.elearning.PlayCourse;
import com.example.elearning.R;
import java.util.ArrayList;
import java.util.List;

public class AdminDashboard extends AppCompatActivity {

    private ListView listView;
    private CourseListAdpater courseListAdpater;
    private String categories[];
    private String titles[];
    private String totalLessonses[];
    private String tutors[];
    private String timestamps[];
    private String tv_res[];

    private List<String> categoryList = new ArrayList<>();
    private List<String> titleList = new ArrayList<>();
    private List<String> totalLessonsList = new ArrayList<>();
    private List<String> tutorList = new ArrayList<>();
    private List<String> timestampList = new ArrayList<>();
    private List<String> tvList = new ArrayList<>();

    SQLiteDatabase db;
    AdminHelper adminHelper = new AdminHelper(this);


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_admin_dashboard);

        listView = findViewById(R.id.lv_admDashboard);
        db = adminHelper.getReadableDatabase();
        Cursor cursor =db.rawQuery("select * from course",null);

        if(cursor.moveToFirst()){
            do {
                int a = cursor.getColumnIndex("category");
                String category = cursor.getString(a);
                int b = cursor.getColumnIndex("title");
                String title = cursor.getString(b);
                int c = cursor.getColumnIndex("totalLessons");
                String totalLessons = cursor.getString(c);
                int d = cursor.getColumnIndex("tutor");
                String tutor = cursor.getString(d);
                int e = cursor.getColumnIndex("timestamp");
                String timestamp = cursor.getString(e);
                int f = cursor.getColumnIndex("course_video");
                String tv_res = cursor.getString(f);
                categoryList.add(category);
                titleList.add(title);
                totalLessonsList.add(totalLessons);
                tutorList.add(tutor);
                timestampList.add(timestamp);
                tvList.add(tv_res);

            }   while (cursor.moveToNext());

    }

        cursor.close();
        db.close();

        categories = categoryList.toArray(new String[0]);
        titles = titleList.toArray(new String[0]);
        totalLessonses = totalLessonsList.toArray(new String[0]);
        tutors= tutorList.toArray(new String[0]);
        timestamps = timestampList.toArray(new String[0]);
        tv_res = tvList.toArray(new String[0]);

       listView = findViewById(R.id.lv_admDashboard);
       courseListAdpater = new CourseListAdpater();
       listView.setAdapter(courseListAdpater);
    }

    class CourseListAdpater extends BaseAdapter {


        @Override
        public int getCount() {
            return categories.length;
        }

        @Override
        public Object getItem(int position) {
            return categories[position];
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            CourseHolder holder = null;
            if(convertView == null){
                convertView = View.inflate(AdminDashboard.this,R.layout.course_list_admin,null);
                holder = new CourseHolder();
                holder.categoryView = convertView.findViewById(R.id.categoryTv);
                holder.titleView = convertView.findViewById(R.id.titleTv);
                holder.tutorView = convertView.findViewById(R.id.tutorTv);
                holder.totalLessonsView = convertView.findViewById(R.id.totlaLessonsTv);
                holder.timeView = convertView.findViewById(R.id.timeTv);
                convertView.findViewById(R.id.iv_delete).setOnTouchListener(new View.OnTouchListener() {
                    @Override
                    public boolean onTouch(View v, MotionEvent event) {
                        startActivity(new Intent(AdminDashboard.this, PlayCourse.class));
                        return false;
                    }
                });

                convertView.setTag(holder);
            }else {
                holder = (CourseHolder) convertView.getTag();
            }

                holder.categoryView.setText(categories[position]);
                holder.titleView.setText(titles[position]);
                holder.tutorView.setText(tutors[position]);
                holder.totalLessonsView.setText(totalLessonses[position]);
                holder.timeView.setText(timestamps[position]);


            return convertView;
        }

        class CourseHolder {
            TextView categoryView,titleView,tutorView,totalLessonsView,timeView;
            //PlayVideo 没有视频资源就不写了
            ImageView imageDelete;

        }


    }


}



