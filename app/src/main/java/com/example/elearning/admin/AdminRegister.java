package com.example.elearning.admin;

import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.content.ContentValues;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.elearning.R;

import java.util.regex.Pattern;

public class AdminRegister extends AppCompatActivity implements View.OnClickListener {

    EditText et_email, et_password, et_confirmPassword, et_username;
    Button btn_Register;
    TextView tv_loginBtn;

    String emailRegex = "^[a-zA-Z0-9_+&*-]+(?:\\." +
            "[a-zA-Z0-9_+&*-]+)*@" +
            "(?:[a-zA-Z0-9-]+\\.)+[a-z" +
            "A-Z]{2,7}$";

    Pattern pat = Pattern.compile(emailRegex);

    AdminHelper adminHelper = new AdminHelper(this);
    SQLiteDatabase db;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_admin_register);

        et_username = findViewById(R.id.et_username);
        et_email = findViewById(R.id.et_email);
        et_password = findViewById(R.id.et_password);
        et_confirmPassword = findViewById(R.id.et_confirmPassword);
        btn_Register = findViewById(R.id.btn_register);
        tv_loginBtn = findViewById(R.id.tv_loginButton);

        tv_loginBtn.setOnClickListener(this);
        btn_Register.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btn_register:
                PerformAuth();
                break;
            case R.id.tv_loginButton:
                startActivity(new Intent(AdminRegister.this, AdminLogin.class));
                break;

        }
    }

    private void PerformAuth(){
        String email = et_email.getText().toString();
        String password = et_password.getText().toString();
        String confirmPassword = et_confirmPassword.getText().toString();
        String username = et_username.getText().toString();

        if (email.isEmpty()) {
            et_email.setError("请输入邮箱");
            return;
        } else if (!pat.matcher(email).matches()) {
            et_email.setError("请输入有效的邮箱");
            return;
        } else if (password.isEmpty()) {
            et_password.setError("请输入密码");
            return;
        } else if (password.length() < 6) {
            et_password.setError("密码太短了");
            return;
        } else if (!confirmPassword.equals(password)) {
            et_confirmPassword.setError("两次密码不一致");
            return;
        } else if(username.isEmpty()) {
            et_username.setError("请输入账户名称");
        }else {

            db = adminHelper.getWritableDatabase();
            ContentValues values = new ContentValues();
            values.put("admin_usename",username);
            values.put("admin_email",email);
            values.put("admin_password",password);
            db.insert("admin",null,values);
            Toast.makeText(AdminRegister.this, "注册成功", Toast.LENGTH_SHORT).show();
            db.close();

        }
    }
}