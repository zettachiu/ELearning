package com.example.elearning.admin;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

import com.example.elearning.R;

public class AdminResetPasswordActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_admin_reset_password);
    }
}