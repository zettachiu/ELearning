package com.example.elearning.admin;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;


public class AdminHelper extends SQLiteOpenHelper {
    private static final String DATABASE_NAME = "ELearning.db";
    //管理员表信息
    public static final String TABLE_ADMIN = "admin";
    public static final String ADMIN_USERNAME = "admin_usename";
    public static final String ADMIN_EMAIL = "admin_email";
    public static final String ADMIN_PASSWORD = "admin_password";

    //教师表信息


    //用户表信息


    //课程表信息
    public static final String TABLE_COURSE = "course";
    public static final String COURSE_CATEGORY = "category";
    public static final String COURSE_TITLE = "title";
    public static final String COURSE_TOTALLESSONS = "totalLessons";
    public static final String COURSE_TUTOR = "tutor";
    public static final String COURSE_TIMESTAMP = "timestamp";
    public static final String COURSE_RES = "course_video";

    public AdminHelper(Context context) {
        super(context, DATABASE_NAME, null, 1);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        //创建管理员账户表
        String createAdminTable = "CREATE TABLE "+ TABLE_ADMIN +"("+ADMIN_USERNAME+" VARCHAR(20),"
                +ADMIN_EMAIL+" VARCHAR(20)"+" PRIMARY KEY ,"
                +ADMIN_PASSWORD+" VARCHAR(20)"+")";

        db.execSQL(createAdminTable);
        //创建课程数据表
        String createCourseTable = "CREATE TABLE "+ TABLE_COURSE +"("+COURSE_CATEGORY+" VARCHAR(20),"
                +COURSE_TITLE+" VARCHAR(20)"+" PRIMARY KEY ,"+COURSE_TOTALLESSONS+" VARCHAR(20),"
                +COURSE_TUTOR+" VARCHAR(20),"+COURSE_TIMESTAMP+" VARCHAR(20),"+COURSE_RES+" VARCHAR(20)"+")";
        db.execSQL(createCourseTable);

        String sql="insert into admin (admin_usename,admin_email,admin_password) values ('Chiu','1391575453@qq.com','123123')";
        db.execSQL(sql);
        String sql1="insert into course (category,title,totalLessons,tutor,timestamp,course_video) values ('前端','零基础学HTML','24课时','jack',null,'cai')";
        db.execSQL(sql1);


    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }
}
