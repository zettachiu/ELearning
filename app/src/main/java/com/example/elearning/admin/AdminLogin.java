package com.example.elearning.admin;


import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.elearning.R;

import java.util.regex.Pattern;

public class AdminLogin extends AppCompatActivity implements View.OnClickListener {

    EditText et_email, et_password;
    Button btn_login;
    TextView tv_registerBtn;

    TextView tv_forgotPassword;

    String emailRegex = "^[a-zA-Z0-9_+&*-]+(?:\\." +
            "[a-zA-Z0-9_+&*-]+)*@" +
            "(?:[a-zA-Z0-9-]+\\.)+[a-z" +
            "A-Z]{2,7}$";

    Pattern pat = Pattern.compile(emailRegex);

//    ProgressDialog progressDialog;

    //数据库创建
    AdminHelper adminHelper = new AdminHelper(this);
    SQLiteDatabase db;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_admin_login);

        et_email = findViewById(R.id.et_email);
        et_password = findViewById(R.id.et_password);
        btn_login = findViewById(R.id.btn_login);
        tv_registerBtn = findViewById(R.id.tv_registerButton);
        tv_forgotPassword = findViewById(R.id.tv_forgotPassword);

        tv_forgotPassword.setOnClickListener(this);
        btn_login.setOnClickListener(this);
        tv_registerBtn.setOnClickListener(this);
//        progressDialog = new ProgressDialog(this);

    }



    @Override
    public void onClick(View v) {

        switch (v.getId()){
            case R.id.tv_forgotPassword:
                startActivity(new Intent(AdminLogin.this, AdminResetPasswordActivity.class));
                break;
            case R.id.btn_login:
                performLogin();
                break;
            case R.id.tv_registerButton:
                startActivity(new Intent(AdminLogin.this, AdminRegister.class));
                break;
            default:

                break;
        }
    }

    private void performLogin() {
        db = adminHelper.getReadableDatabase();
        String email = et_email.getText().toString();
        String password = et_password.getText().toString();


        if (email.isEmpty()) {
            et_email.setError("请输入邮箱");
            return;
        } else if (!pat.matcher(email).matches()) {
            et_email.setError("请输入有效的电子邮箱");
            return;
        } else if (password.isEmpty()) {
            et_password.setError("请输入密码");
            return;
        } else if (password.length() < 6) {
            et_password.setError("密码长度过短");
            return;
        } else {
//            progressDialog.setMessage("登录你的账户....");
//            progressDialog.setTitle("登录中");
//            progressDialog.setCanceledOnTouchOutside(false);
//            progressDialog.show();


        String sql = "select * from admin where admin_email=? and admin_password=?";
        Cursor cursor = db.rawQuery(sql,new String[]{email,password});
        if(cursor.moveToFirst()){
//            progressDialog.dismiss();
            Toast.makeText(AdminLogin.this, "登录成功！", Toast.LENGTH_SHORT).show();
            startActivity(new Intent(AdminLogin.this, AdminDashboard.class));
        }else {
//            progressDialog.dismiss();
            Toast.makeText(AdminLogin.this, "登陆失败！邮箱或密码错误！", Toast.LENGTH_SHORT).show();

        }

        }
        db.close();
    }

}